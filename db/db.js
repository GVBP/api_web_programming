const ObjectId = require('mongodb').ObjectID;
const MongoClient = require('mongodb').MongoClient;
const uri = "mongodb+srv://new-user_31:PxgUgHr06RtoZ6Dr@clusterfirst.8lyem.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
let collection;

client.connect(err => {
    if (err) return console.log(err);
    collection = client.db("api_web").collection("animals");
});

function saveAnimal(animal, callback){
    collection.insertOne(animal, callback);
}

function updateAnimal(id, object, callback){
    collection.updateOne({ "_id" : ObjectId(id) }, { $set : object });
    getById(id, callback);
}

function deleteAnimal(id, callback){
    collection.deleteOne({ "_id" : ObjectId(id) }, callback);
}

function getAll(callback){
    collection.find({}).toArray(callback);
}

function getById(id, callback){
    collection.findOne({ "_id" : ObjectId(id) }, callback);
}

function getByDescription(animalDescription, callback){
    collection.find({ "animalDescription" : animalDescription }).toArray(callback);
}

function getByGroup(groupAnimals, callback){
    collection.find({ "groupAnimals" : groupAnimals }).toArray(callback);
}

function getByClass(classAnimals, callback){
    collection.find({ "classAnimals" : classAnimals }).toArray(callback);
}

module.exports = {
    saveAnimal,
    updateAnimal,
    deleteAnimal,
    getAll,
    getById,
    getByDescription,
    getByGroup,
    getByClass
}
