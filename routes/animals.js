var express = require('express');
var router = express.Router();

/* POST */
router.post('/', function(req, res, next) {
    const animalDescription = req.body.animalDescription;
    const groupAnimals = req.body.groupAnimals;
    const classAnimals = req.body.classAnimals;
    
    global.db.saveAnimal({animalDescription, groupAnimals, classAnimals}, (err, result) => {
        if(err) { return console.log(err); }
        var jsonResponse = {
            "_id" : result.insertedId,
            "animalDescription" : animalDescription,
            "groupAnimals" : groupAnimals,
            "classAnimals" : classAnimals
        }
        res.json(jsonResponse);
    });
});

/* UPDATE */
router.put('/:id', function(req, res, next) {
    var id = req.params.id;
    const object = req.body[0];

    global.db.updateAnimal(id, object, (err, jsonResponse) => {
        if(err) { return console.log(err); }
        res.json(jsonResponse);
    });
});

/* DELETE */
router.delete('/:id', function(req, res, next) {
    var id = req.params.id;
    
    global.db.deleteAnimal(id, (e, r) => {
        if(e) { return console.log(e); }
        res.sendStatus(200);
    });
});

/* GET */
router.get('/', function(req, res, next) {
    global.db.getAll((e, jsonResponse) => {
        if(e) { return console.log(e); }
        res.json(jsonResponse);
    });
});

/* GET */
router.get('/:id', function(req, res, next) {
    var id = req.params.id;

    global.db.getById(id, (e, jsonResponse) => {
        if(e) { return console.log(e); }
        res.json(jsonResponse);
    });
});

/* GET */
router.get('/description/:animalDescription', function(req, res, next) {
    const animalDescription = decodeURI(req.params.animalDescription);
    console.log(animalDescription);

    global.db.getByDescription(animalDescription, (e, jsonResponse) => {
        if(e) { return console.log(e); }
        res.json(jsonResponse);
    });
});

/* GET */
router.get('/group/:groupAnimals', function(req, res, next) {
    const groupAnimals = decodeURI(req.params.groupAnimals);

    global.db.getByGroup(groupAnimals, (e, jsonResponse) => {
        if(e) { return console.log(e); }
        res.json(jsonResponse);
    });
});

/* GET */
router.get('/class/:classAnimals', function(req, res, next) {
    const classAnimals = decodeURI(req.params.classAnimals);

    global.db.getByClass(classAnimals, (e, jsonResponse) => {
        if(e) { return console.log(e); }
        res.json(jsonResponse);
    });
});
  
module.exports = router;