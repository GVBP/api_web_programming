<p align="center">
  <a href="https://unform.dev">
    <img src="https://cdn4.iconfinder.com/data/icons/web-development-5/500/api-code-window-256.png" height="128" width="128" alt="Unform" />
  </a>
</p>

<h1 align="center">API Web Teste</h1>

<p align="center">Simples atividade sobre API Web</p>

<p align="center">
  <img src="https://img.shields.io/static/v1?label=Projeto&message=PUCSC&color=red&style=for-the-badge&logo=ghost"/>
</p>

Tabela de conteúdos
=================
<!--ts-->
   * Sobre
   * Tabela de Conteúdo
   * Features
   * Demonstração da aplicação
   * Pré-requisitos
      * Rodando o Back End (servidor)
      * Rodando o Postman
         * Rodando o saveAnimal
         * Rodando o getAll
         * Rodando o getById
         * Rodando o getByDescription
         * Rodando o getByGroup
         * Rodando o getByClass
         * Rodando o updateAnimal
         * Rodando o deleteAnimal
   * Tecnologias
   * Autor
<!--te-->

<h1 align="center">Status do Projeto</h1>
<h4 align="center"> 
  ✅ Finalizado
</h4>

# Features

- [x] API - Cadastrar um animal
- [x] API - Atualizar um animal
- [x] API - Excluir um animal
- [x] API - Obter um animal pelo id
- [x] API - Obter um animal pela descrição
- [x] API - Obter um animal pelo grupo
- [x] API - Obter um animal pela classe

# Demonstração da aplicação

![saveAnimal](screenshots/saveAnimal.png)
![updateAnimal](screenshots/updateAnimal.png)
![deleteAnimal](screenshots/deleteAnimal.png)
![getAll](screenshots/getAll.png)
![getById](screenshots/getById.png)
![getByDescription](screenshots/getByDescription.png)
![getByGroup](screenshots/getByGroup.png)
![getByClass](screenshots/getByClass.png)

# Pré-requisitos

Antes de começar, você vai precisar ter instalado em sua máquina as seguintes ferramentas:
[Git](https://git-scm.com), [Node.js](https://nodejs.org/en/), [Postman](https://www.postman.com/). 
Além disto é bom ter um editor para trabalhar com o código como [VSCode](https://code.visualstudio.com/).

## 🎲 Rodando o Back End (servidor)

```bash
# Clone este repositório
$ git clone <https://github.com/GVBP/api_web_programming>

# Acesse a pasta do projeto no terminal/cmd
$ cd api_web_programming

# Instale as dependências
$ npm install

# Execute a aplicação
$ npm start

# O servidor inciará na porta:3000
```

## 🎲 Rodando o Postman

```bash
# Acesse a pasta do projeto no terminal/cmd
$ cd caminho/do/projeto/api_web_programming

# Acesse a pasta postman
$ cd postman

# Importe o json para dentro da aplicação do Postman
```

### 🎲 Rodando o saveAnimal

```http
# Para tal, deve ser enviado ao corpo dessa requisição as chaves (animalDescription, groupAnimals e classAnimals) com seus respectivos valores.

# Código exemplo:

POST /animals HTTP/1.1
Host: localhost:3000
Content-Type: application/json
Content-Length: 103

{
    "animalDescription" : "Leão",
    "groupAnimals" : "Terrestre",
    "classAnimals" : "Mamífero"
}
```

### 🎲 Rodando o getAll

```http
# Para tal, deve ser apenas executado a requisição.

# Código exemplo:

GET /animals HTTP/1.1
Host: localhost:3000
```

### 🎲 Rodando o getById

```http
# Para tal, deve ser informado ao parâmetro o id.

# Código exemplo:

GET /animals/{{_id}} HTTP/1.1
Host: localhost:3000
```

### 🎲 Rodando o getByDescription

```http
# Para tal, deve ser informado ao parâmetro a descrição.

# Código exemplo:

GET /animals/description/{{animalDescription}} HTTP/1.1
Host: localhost:3000
```

### 🎲 Rodando o getByGroup

```http
# Para tal, deve ser informado ao parâmetro o grupo.

# Código exemplo:

GET /animals/group/{{groupAnimals}} HTTP/1.1
Host: localhost:3000
```

### 🎲 Rodando o getByClass

```http
# Para tal, deve ser informado ao parâmetro o classe.

# Código exemplo:

GET /animals/class/{{classAnimals}} HTTP/1.1
Host: localhost:3000
```

### 🎲 Rodando o updateAnimal

```http
# Para tal, deve ser informado no parâmetro o id e deve ser enviado no corpo dessa requisição ao menos uma ou todas as chaves (animalDescription, groupAnimals e classAnimals) com seus respectivos valores.

# Código exemplo:

PUT /animals/{{_id}} HTTP/1.1
Host: localhost:3000
Content-Type: application/json
Content-Length: 51

[
    {
        "animalDescription": "Leoa"
    }
]
```

### 🎲 Rodando o deleteAnimal

```http
# Para tal, deve ser informado ao parâmetro o id.

# Código exemplo:

DELETE /animals/{{_id}} HTTP/1.1
Host: localhost:3000
```

# 🛠 Tecnologias

As seguintes ferramentas foram usadas na construção do projeto:

- [Node.js](https://nodejs.org/en/)

# Autor

<div>
  <img style="border-radius: 50%;" src="https://avatars.githubusercontent.com/u/38007365?s=480&v=4" width="100px;" alt=""/>
  <br />
  <sub>
    <b>Guilherme Pereira</b>
  </sub>
</div>
